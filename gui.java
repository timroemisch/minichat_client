
/**
 * Write a description of class gui here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class gui {
    private TextfensterFarbe chatLB;    
    private Eingabefeld input;
    private Taste sendBtn;
    private Kreis onlineImg;
    
    private Eingabefeld server, port;
    private Taste connectBtn;
    private Taste disconnectBtn;
    private Taste clear;
    private int listHeight = 20;

    public gui(CLIENT parent) {
                
        Zeichnung.setzeFenstergroesse(525, 430);
        Zeichnung.setzeScrollbar(false);
        Zeichnung.setzeTitel("Simple BlueJ Chat - Offline");
        
        
        new Ausgabe("Server", 10, 5, 40, 10).setzeSchriftgroesse(12);
        server = new Eingabefeld("", 10, 20, 100, 20);
        server.setzeAusgabetext("10.10.50.");
        server.setzeSchriftgroesse(12);
        server.setzeLink(parent, 30);

        new Ausgabe("Port", 110, 5, 30, 10).setzeSchriftgroesse(12);
        port = new Eingabefeld("", 110, 20, 100, 20);
        port.setzeAusgabetext("1234");
        port.setzeSchriftgroesse(12);
        port.setzeLink(parent, 30);
        
        connectBtn = new Taste("Connect", 220, 20, 80, 20);
        connectBtn.setzeSchriftgroesse(12);
        connectBtn.setzeLink(parent, 30); // open connection
        
        disconnectBtn = new Taste("Disconnect", 400, 20, 100, 20);
        disconnectBtn.setzeSchriftgroesse(12);
        disconnectBtn.setzeLink(parent, 40); // close connection
        
        clear = new Taste("Clear", 310, 20, 80, 20);
        clear.setzeSchriftgroesse(12);
        clear.setzeLink(parent, 50); // close connection
        
        chatLB = new TextfensterFarbe(10, 50, 500, 300);
        
        input = new Eingabefeld("", 30, 355, 400, 20);
        input.setzeSchriftgroesse(12);
        input.setzeAusgabetext("");
        input.setzeLink(parent, 20); // if enter is pressed send text
        
        sendBtn = new Taste("Senden", 430, 355, 80, 20);
        sendBtn.setzeSchriftgroesse(12);
        sendBtn.setzeLink(parent, 20); // if button is pressed send text
        
        onlineImg = new Kreis(10, 355, 10);
        onlineImg.setzeFarbe("rot");
        
      
    }
    
    
    public String getInput() {
        String in = input.toString();
        input.setzeAusgabetext("");
        
        return in;
    }
    
    public void write(String i) {
        chatLB.println(i);
    }
    
    public void setOnlineStatus(boolean s) {
        if (s) {
            onlineImg.setzeFarbe("gruen");
            Zeichnung.setzeTitel("Simple BlueJ Chat - Online");
        }
        else {
            onlineImg.setzeFarbe("rot");
            Zeichnung.setzeTitel("Simple BlueJ Chat - Offline");
        }
    }
    
    public String getServer() {
        return server.toString();
    }
    
    public int getPort() {
        return port.leseInteger(1234);
    }
    
    public void clearChat() {
        chatLB.clear();
    }
    
}
