
import java.io.*;
import java.net.*;

/**
 * Erste Clientimplementierung des Projekts wie gehts?
 * @author ISB-Arbeitskreis, Umsetzungshilfen Informatik 12
 * @version 1.0
 */
public class CLIENT implements ITuWas {

    //socket
    private Socket clientSocket = null;
    
    //io
    private PrintWriter zumServer = null;
    private BufferedReader vomServer = null;

    private Taktgeber takt; //f�r ping
    private receive rec; // event handler for new input

    private boolean running = false;
    private gui ui;

    /**
     * Konstruktor
     * @exception IOException eine Ausnahme tritt m&ouml;glicherweise auf falls:<br/>
     * - die Clientverbindung nicht hergestellt werden konnte (beispielsweise bei falscher IP-Adresse oder
     * falschem Port)<br/>
     * - die Verbindung zum Server gest&ouml;rt bzw. unterbrochen wurde.
     */
    public CLIENT() {

        ui = new gui(this); // load gui
        
        while(true){ 

            boolean connected = false; // is only true if there is a connection

            while (!connected) { // wait while there is no conn

                while(!running) { // wait for user to start conn
                    StaticTools.warte(1);
                }

                // try connecting
                // if there is an error go back
                try {
                    VerbindungHerstellen();
                    connected = true;
                }
                catch (Exception e){
                    connected = false;
                    running = false;
                    ui.write("Error bei der Verbindung !");
                }
                
            }

            //empf�nger
            rec = new receive(vomServer, ui);
            rec.start();

            //taktgeber f�r ping
            takt = new Taktgeber(this, 10);
            takt.endlos(0,1000);

            //warte bis erster ping erfolgreich ist
            while (!heartbeat.isAlive()) {
                StaticTools.warte(1);
            }

            //main loop
            //bleibt so lange im loop wie verbunden
            do {
                ui.setOnlineStatus(true);
                StaticTools.warte(1000); // slow loop down
            } while (heartbeat.isAlive());

            close(); //close session
        }
    }

    private void close() {
        ui.setOnlineStatus(false); //gui info
        takt.stop(); // ping stop
        rec.close(); // stop receiver

        try {
            // close stream
            zumServer.close();
            vomServer.close();  

            // close socket
            clientSocket.close();
        }
        catch(IOException e) {
            System.out.println("Error "+e);
        }

        // msg
        ui.write("Disconnected from Server !");

        //reset running to false
        running = false;
    }

    public void tuWas(int id) {
        if (id == 10) {
            zumServer.println("/ping"); //send ping
        }
        else if (id == 20) {
            zumServer.println(ui.getInput()); //send text from gui
        }
        else if (id == 30) {
            running = true; //open connection
        } 
        else if (id == 40) {
            //close connection
            zumServer.println("/exit"); //send exit
            close();
        } 
        else if (id == 50) {
            ui.clearChat();
        }
    }

    /**
     * stellt die Serververbindung her und erzeugt die n&ouml;tigen Lese- und Schreibojekte
     * @exception IOException tritt auf, falls die Verbindung nicht korrekt erstellt werden kann.
     */
    private void VerbindungHerstellen() throws IOException {
        //Ipadresse und Port ermitteln
        String ipadresse = ui.getServer();
        int port = ui.getPort();

        //Verbindung herstellen
        clientSocket = new Socket(ipadresse, port);
        zumServer = new PrintWriter(clientSocket.getOutputStream(), true);
        vomServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

    }

    /**
     * Hauptprogramm zum Erzeugen eines Clientobjekts
     * @param args keine Parameter beim Programmaufruf erforderlich
     */
    public static void main(String[] args) {

        try {
            new CLIENT();
        } catch (Exception e) {
            System.out.println("Fehler im Clientprogramm.");
            System.exit(1);
        }
    }
}
