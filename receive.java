import java.io.*;

/**
 * Write a description of class receive here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class receive extends Thread{
    private String in;
    private BufferedReader vomServer = null;
    private boolean running = false;
    private gui ui;
    
    public receive(BufferedReader vS, gui userinterface) {
        //save params
        vomServer = vS;
        ui = userinterface;
        
        //set running
        running = true;
    }
    
    public void run() {
        while (running) {
            try {
                in = vomServer.readLine(); // wait for server
                if (in == null) break;
                
                // check if Server response is an command
                if (in.startsWith("/")) {
                  if (in.equalsIgnoreCase("/pong")) { 
                      // if we got a pong we can update the heartbeat
                      heartbeat.gotPong();
                  }
                  else {
                      ui.write("Got Unknown Command: " + in);
                  }
                }
                else {
                    // no command => simple text
                    ui.write(in);
                }
            }
            catch(IOException e) {
                System.out.println("Error "+e);
            }
        }
    }
    
    public void close() {
        running = false;
    }    

}
