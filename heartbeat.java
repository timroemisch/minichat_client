import java.io.*;

/**
 * Write a description of class heartbeat here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class heartbeat {
    private static long lastHB = 0;
    
    public heartbeat() {
    }

    public static void gotPong() {
        lastHB = System.currentTimeMillis();
    }
    
    public static boolean isAlive() {
        return (lastHB > System.currentTimeMillis() - 1500);
    }

}
